#-------------------------------------------------
#
# Project created by QtCreator 2013-03-01T16:06:31
#
#-------------------------------------------------

win32:{
GIT_VERSION = $$system( "C:/Program Files/Git/cmd/git.exe" --git-dir $$PWD/.git --work-tree $$PWD describe --always --tags )
}
else{
GIT_VERSION = $$system( git --git-dir $$PWD/.git --work-tree $$PWD describe --always --tags )
}
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

DEFINES += "\"FIRTOOL_VERSION=2.0\""

QT += core gui widgets printsupport

TARGET = FIRTool
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# configs
CONFIG += rtti
CONFIG += c++11
CONFIG += c++14

include ( appimage.pri )

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    freqchar.cpp \
    filters.cpp \
    fftw.cpp \
    qmtxsdrlubber.cpp \
    windowCheby.cpp \
    windowKaiser.cpp \
    windowHarris.cpp \
    windowRifeVincent.cpp \
    remez.cpp \
    newton_real.cpp \
    qcustomplot.cpp

HEADERS += \
    mainwindow.h \
    filters.h \
    fftw.h \
    qmtxsdrlubber.h \
    windowRifeVincent.h \
    windowHarris.h \
    freqchar.h \
    windowCheby.h \
    windowKaiser.h \
    remez.h \
    double.h \
    qcustomplot.h

FORMS +=  \
    mainwindow.ui

RESOURCES += \
    firtool.qrc

macx : {
    ICON = toolbox.ico
}

win32 : {
    OTHER_FILES += firtool.rc
    RC_FILE = firtool.rc

    INCLUDEPATH += ../fftw-3.3.5-dll64
    LIBS += -L../fftw-3.3.5-dll64 -lfftw3l-3

    QMAKE_LFLAGS += -static-libgcc -static-libstdc++
}

linux : {
    LIBS += -L/usr/lib/ -lfftw3l
}

mac : {
    INCLUDEPATH += /usr/local/include
    DEPENDPATH += /usr/local/include
    LIBS += -L/usr/local/lib/ -lfftw3l
}



