
/*
    copyright © 2003 - 2020 : Henk van Kampen <henk@mediatronix.com>

    This file is part of firtool.

    firtool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    firtool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with firtool.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow( QWidget* parent ) : QMainWindow( parent ), ui( new Ui::MainWindow ) {
    ui->setupUi( this );

    // setup window and app name and versions
    this->setWindowIcon( QIcon( ":/Icons/toolbox.ico" ) );
    qApp->setWindowIcon( QIcon( ":/Icons/toolbox.ico" ) );

#if( defined( __GNUC__ ) || defined( __GNUG__ ) ) && !( defined( __clang__ ) || defined( __INTEL_COMPILER ) )
    QString compiler( "GCC " __VERSION__ );
#elif defined( __clang__ )
    QString compiler( "clang " __clang_version__ );
#elif defined( _MSC_FULL_VER )
    QString compiler( "MSC " _MSC_FULL_VER );
#endif
    auto version = QString(
                       "FIRTool Version: %1"
                       " -- Git Hash: %2"
                       " -- Qt Libs: %3"
                       " -- Compiler: %4" )
                       .arg( FIRTOOL_VERSION )
                       .arg( GIT_VERSION )
                       .arg( QT_VERSION_STR )
                       .arg( compiler );
    this->setWindowTitle( version );
    qApp->setApplicationName( version );

    // set dock actions
    ui->menuView->addAction( ui->dwTypes->toggleViewAction() );
    ui->menuView->addAction( ui->dwMethods->toggleViewAction() );
    ui->menuView->addAction( ui->dwCoefficients->toggleViewAction() );

    // set tab widgets
    ui->twMain->setCurrentIndex( 0 );
    ui->twMethod->setCurrentIndex( 0 );
    ui->twType->setCurrentIndex( 0 );

    ui->cbFilterType->addItems(
        QStringList() << " Taylor"
                      << " Rife-Vincent"
                      << " harris4"
                      << " harris7"
                      << " Nuttall"
                      << " Flat top"
                      << " Connes" );

    ui->twCoefs->setHeaderLabels(
        QStringList() << "Index"
                      << "Coefs"
                      << "Actual"
                      << "Window" );


    ui->cbFilterType->setCurrentIndex( 0 );
    ui->dsbSubtype->setValue( 0 );

    ui->twCoefs->setColumnWidth( 0, 60 );
    ui->twCoefs->setColumnWidth( 1, 100 );

    // exit
    connect( ui->actionExit, &QAction::triggered, this, &QWidget::close );

    // standard
    connect( ui->actionAbout_Qt, &QAction::triggered, qApp, &QApplication::aboutQt );

    loadSavePath = QDir::currentPath();
    readSettings();
}

MainWindow::~MainWindow() {
    writeSettings();
    delete ui;
}

bool MainWindow::isLogaritmic( void ) {
    return ui->cbLog->isChecked();
}

void MainWindow::makeFreqGraph( void ) {
    if( !customPlotFreq.isNull() )
        ui->loMagnitude->removeWidget( customPlotFreq );

    customPlotFreq = new QCustomPlot();
    ui->loMagnitude->addWidget( customPlotFreq );

    customPlotFreq->setInteractions(
        QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables );

    customPlotFreq->axisRect( 0 )->setupFullAxesBox( true );
    customPlotFreq->setNoAntialiasingOnDrag( true );

    auto line = new QCPItemLine( customPlotFreq );
    if( isLogaritmic() ) {
        line->start->setCoords( 0.0, -6 );
        line->end->setCoords( 0.5, -6 );
    } else {
        line->start->setCoords( 0.0, 0.501 );
        line->end->setCoords( 0.5, 0.501 );
    }
    line->setPen( QPen( Qt::red ) );

    fixedTicker.reset( new QCPAxisTickerFixed );
    fixedTicker->setTickStep( 0.05 );
    fixedTicker->setScaleStrategy( QCPAxisTickerFixed::ssPowers );

    customPlotFreq->xAxis->setRange( QCPRange( ui->dsbXRangeLow->value(), ui->dsbXRangeHigh->value() ) );
    customPlotFreq->yAxis->setRange( QCPRange( ui->dsbRangeLow->value(), ui->dsbRangeHigh->value() ) );

    customPlotFreq->addGraph();
    customPlotFreq->graph( 0 )->setPen( QPen( Qt::blue ) );
    customPlotFreq->addGraph();
    customPlotFreq->graph( 1 )->setPen( QPen( Qt::red ) );
    customPlotFreq->addGraph();
    customPlotFreq->graph( 2 )->setPen( QPen( Qt::green ) );
    customPlotFreq->xAxis->setTicker( fixedTicker );

    customPlotFreq->setAntialiasedElements( QCP::aeNone );
    customPlotFreq->setNotAntialiasedElements( QCP::aeAll );

    customPlotFreq->xAxis->grid()->setSubGridVisible( true );
    customPlotFreq->yAxis->grid()->setSubGridVisible( true );

    connect( customPlotFreq, &QCustomPlot::mouseWheel, [this]( QWheelEvent* event ) {
        if( event->modifiers() & Qt::ControlModifier )
            customPlotFreq->axisRect( 0 )->setRangeZoom( Qt::Vertical );
        else if( event->modifiers() & Qt::ShiftModifier )
            customPlotFreq->axisRect( 0 )->setRangeZoom( Qt::Horizontal );
        else
            customPlotFreq->axisRect( 0 )->setRangeZoom( Qt::Horizontal | Qt::Vertical );
    } );

    connect( customPlotFreq->xAxis, SIGNAL( rangeChanged( QCPRange ) ), this, SLOT( updateXRangeBoxes( QCPRange ) ) );
    connect( customPlotFreq->yAxis, SIGNAL( rangeChanged( QCPRange ) ), this, SLOT( updateYRangeBoxes( QCPRange ) ) );
}

void MainWindow::updateYRangeBoxes( QCPRange newRange ) {
    if( isLogaritmic() ) {
        ui->dsbRangeLow->setValue( newRange.lower );
        ui->dsbRangeHigh->setValue( newRange.upper );
    }
}

void MainWindow::updateXRangeBoxes( QCPRange newRange ) {
    ui->dsbXRangeLow->setValue( newRange.lower );
    ui->dsbXRangeHigh->setValue( newRange.upper );
}

void MainWindow::makeTimeGraph( void ) {
    if( !customPlotTime.isNull() )
        ui->loTime->removeWidget( customPlotTime );

    customPlotTime = new QCustomPlot();
    ui->loTime->addWidget( customPlotTime );

    customPlotTime->axisRect( 0 )->setupFullAxesBox( true );
    customPlotTime->setNoAntialiasingOnDrag( true );

    customPlotTime->addGraph();
    customPlotTime->graph( 0 )->setPen( QPen( Qt::lightGray ) );
    customPlotTime->addGraph();
    customPlotTime->graph( 1 )->setPen( QPen( Qt::blue ) );

    QCPScatterStyle myScatter;
    myScatter.setShape( QCPScatterStyle::ssCircle );
    myScatter.setPen( QPen( Qt::blue ) );
    myScatter.setBrush( Qt::white );
    myScatter.setSize( 4 );
    customPlotTime->graph( 1 )->setScatterStyle( myScatter );

    customPlotTime->graph( 1 )->setLineStyle( QCPGraph::lsImpulse );

    // configure right and top axis to show ticks but no labels:
    QCPAxisRect* wideAxisRect = new QCPAxisRect( customPlotTime );
    wideAxisRect->setupFullAxesBox( true );

    // allow selection of graphs via mouse click
    customPlotTime->setInteractions(
        QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables );

    customPlotTime->xAxis->grid()->setSubGridVisible( true );
    customPlotTime->yAxis->grid()->setSubGridVisible( true );

    connect( customPlotTime, &QCustomPlot::mouseWheel, [this]( QWheelEvent* event ) {
        if( event->modifiers() & Qt::ControlModifier )
            customPlotTime->axisRect( 0 )->setRangeZoom( Qt::Vertical );
        else if( event->modifiers() & Qt::ShiftModifier )
            customPlotTime->axisRect( 0 )->setRangeZoom( Qt::Horizontal );
        else
            customPlotTime->axisRect( 0 )->setRangeZoom( Qt::Horizontal | Qt::Vertical );
    } );
}

void MainWindow::makePoleGraph( void ) {
    if( !customPlotZero.isNull() )
        ui->loZero->removeWidget( customPlotZero );
    customPlotZero = new QCustomPlot();
    ui->loZero->addWidget( customPlotZero );

    customPlotZero->setMinimumWidth( customPlotZero->size().width() );
    customPlotZero->axisRect( 0 )->setupFullAxesBox();
    customPlotZero->xAxis->setRange( QCPRange( -1.3, 1.3 ) );
    customPlotZero->yAxis->setRange( QCPRange( -1.3, 1.3 ) );
    customPlotZero->xAxis->grid()->setSubGridVisible( true );
    customPlotZero->yAxis->grid()->setSubGridVisible( true );

    customPlotZero->addGraph();
    customPlotZero->graph( 0 )->setScatterStyle( QCPScatterStyle::ssCircle );

    customPlotZero->graph( 0 )->setLineStyle( QCPGraph::lsNone );
    customPlotZero->axisRect( 0 )->setRangeDrag( Qt::Horizontal | Qt::Vertical );
    customPlotZero->axisRect( 0 )->setRangeZoom( Qt::Horizontal | Qt::Vertical );

    QCPItemEllipse* unitcircle = new QCPItemEllipse( customPlotZero );
    unitcircle->setPen( QPen( Qt::gray ) );
    unitcircle->topLeft->setCoords( 1, -1 );
    unitcircle->bottomRight->setCoords( -1, 1 );
}

void MainWindow::readSettings( void ) {
    QSettings settings( QSettings::IniFormat, QSettings::UserScope, "Mediatronix", "firtool" );

    settings.beginGroup( "mainwindow" );
    restoreGeometry( settings.value( "geometry" ).toByteArray() );
    restoreState( settings.value( "state" ).toByteArray() );
    settings.endGroup();
}

void MainWindow::writeSettings( void ) {
    QSettings settings( QSettings::IniFormat, QSettings::UserScope, "Mediatronix", "firtool" );

    settings.clear();
    settings.sync();

    settings.beginGroup( "mainwindow" );
    settings.setValue( "state", saveState() );
    settings.setValue( "geometry", saveGeometry() );
    settings.endGroup();

    settings.sync();
}

QVector<ld_t> MainWindow::reshape( const QVector<ld_t> inV, const int m, bool reordered, bool rev ) {
    QVector<ld_t> coefs;
    auto V = inV;

    // create a matrix of m rows
    std::vector<std::vector<ld_t>> matrix( m );

    // reshape 1d to 2d
    while( V.size() > 0 )
        for( auto& row : matrix ) {
            if( V.size() > 0 )
                row.push_back( V.takeFirst() );
        }

    // reverse the order of the rows
    if( rev )
        std::reverse( matrix.begin(), matrix.end() );

    // reorder the rows half way
    if( reordered )
        std::rotate( matrix.begin(), matrix.begin() + m / 2, matrix.end() );

    for( auto& row : matrix )
        for( auto coef : row )
            coefs.push_back( coef );

    return coefs;
}

void MainWindow::on_actionAbout_triggered() {
    QMessageBox::about(
        this, "FIRTool",
        windowTitle() + "\n\nThis program comes with ABSOLUTELY NO WARRANTY.\n" +
            "This is free software, and you are welcome to redistribute it " +
            "under certain conditions. See <http://www.gnu.org/licenses/>" );
}

void MainWindow::oddTaps( void ) {
    nTaps = ui->sbNTaps->value() /*| 1*/;
    setTaps( nTaps );
}

void MainWindow::on_actionDesign_triggered() {
    Window.clear();
    Coefs.clear();

    nTaps = ui->sbNTaps->value();

    switch( ui->twMethod->currentIndex() ) {
    case 0: {
        oddTaps();
        doCheby();
    } break;
    case 1: {
        oddTaps();
        doKaiser();
    } break;
    case 2: {
        oddTaps();
        doRemez();
    } break;
    case 3: {
        oddTaps();
        doWindowed();
    } break;
    case 4: {
        oddTaps();
        doRRC();
    } break;
    case 5: {
        oddTaps();
        doGauss();
    } break;
    case 6: {
        doLubber();
    } break;
    }
    doShow( Coefs );
    setCoefsWidget( Coefs );
}

void MainWindow::doCheby( void ) {
    Window.resize( nTaps );
    for( int n = 0; n < nTaps; n += 1 )
        Window[n] = 0.0L;

    double beta = ui->dsbChebyBeta->value();
    chebyshev( Window.data(), nTaps, beta );

    // filter type
    Coefs.resize( nTaps );
    switch( ui->twType->currentIndex() ) {
    case 0: lowPass( Coefs.data(), ui->dsbLPStop->value(), Window.data(), nTaps ); break;
    case 1: highPass( Coefs.data(), ui->dsbHPStart->value(), Window.data(), nTaps ); break;
    case 2: bandPass( Coefs.data(), ui->dsbBPStart->value(), ui->dsbBPStop->value(), Window.data(), nTaps ); break;
    case 3: bandStop( Coefs.data(), ui->dsbBSStop->value(), ui->dsbBSStart->value(), Window.data(), nTaps ); break;
    default: statusBar()->showMessage( "Chebyshev: <not implemented yet>", 2000 );
    }
}

void MainWindow::doKaiser( void ) {
    Window.resize( nTaps );
    for( int n = 0; n < nTaps; n += 1 )
        Window[n] = 0.0L;

    double beta = ui->dsbKaiserBeta->value();
    kaiser( Window.data(), nTaps, beta );

    // filter type
    Coefs.resize( nTaps );
    switch( ui->twType->currentIndex() ) {
    case 0: lowPass( Coefs.data(), ui->dsbLPStop->value(), Window.data(), nTaps ); break;
    case 1: highPass( Coefs.data(), ui->dsbHPStart->value(), Window.data(), nTaps ); break;
    case 2: bandPass( Coefs.data(), ui->dsbBPStart->value(), ui->dsbBPStop->value(), Window.data(), nTaps ); break;
    case 3: bandStop( Coefs.data(), ui->dsbBSStop->value(), ui->dsbBSStart->value(), Window.data(), nTaps ); break;
    case 6: lowPass( Coefs.data(), ui->dsbLPStop->value() / ui->sbChannels->value(), Window.data(), nTaps ); break;
    default: statusBar()->showMessage( "Kaiser: <not implemented yet>", 2000 );
    }
}

void MainWindow::doWindowed( void ) {
    Window.resize( nTaps );
    for( int n = 0; n < nTaps; n += 1 )
        Window[n] = 0.0L;

    double atten = ui->dsbAtten->value();

    // filter window
    switch( ui->cbFilterType->currentIndex() ) {
    case 0: rifeVincentII( Window.data(), nTaps, atten, ( int )ui->dsbSubtype->value() ); break;
    case 1: rifeVincent( ( RV_t )ui->dsbSubtype->value(), Window.data(), nTaps ); break;
    case 2: harris4( Window.data(), nTaps ); break;
    case 3: harris7( Window.data(), nTaps ); break;
    case 4: nuttall( Window.data(), nTaps ); break;
    case 5: flattop( Window.data(), nTaps ); break;
    case 6: connes( Window.data(), nTaps ); break;
    }

    // filter type
    Coefs.resize( nTaps );
    switch( ui->twType->currentIndex() ) {
    case 0: lowPass( Coefs.data(), ui->dsbLPStop->value(), Window.data(), nTaps ); break;
    case 1: highPass( Coefs.data(), ui->dsbHPStart->value(), Window.data(), nTaps ); break;
    case 2: bandPass( Coefs.data(), ui->dsbBPStart->value(), ui->dsbBPStop->value(), Window.data(), nTaps ); break;
    case 3: bandStop( Coefs.data(), ui->dsbBSStop->value(), ui->dsbBSStart->value(), Window.data(), nTaps ); break;
    default: statusBar()->showMessage( "Windowed: <not implemented yet>", 2000 );
    }
}

void MainWindow::doRemez( void ) {
    QVector<ld_t> bands;
    QVector<ld_t> response;
    QVector<ld_t> weight;
    int nbands = 0;

    double tw = ui->dsbTW->value() / 2.0;
    double tf, tfl, tfr;
    REMEZ_t type = BANDPASS;

    switch( ui->twType->currentIndex() ) {
    case 0:
        nbands = 2;
        tf = ui->dsbLPStop->value();
        bands << 0.0 << tf - tw << tf + tw << 0.5;
        response << 1 << 1 << 0 << 0;
        weight << 1 << 1;
        type = BANDPASS;
        break;
    case 1:
        nbands = 2;
        tf = ui->dsbHPStart->value();
        bands << 0.0 << tf - tw << tf + tw << 0.5;
        response << 0 << 0 << 1 << 1;
        weight << 1 << 1;
        type = BANDPASS;
        break;
    case 2:
        nbands = 3;
        tfl = ui->dsbBPStart->value();
        tfr = ui->dsbBPStop->value();
        bands << 0.0 << tfl - tw << tfl + tw << tfr - tw << tfr + tw << 0.5;
        response << 0 << 0 << 1 << 1 << 0 << 0;
        weight << 1 << 1 << 1;
        type = BANDPASS;
        break;
    case 3:
        nbands = 3;
        tfl = ui->dsbBSStop->value();
        tfr = ui->dsbBSStart->value();
        bands << 0.0 << tfl - tw << tfl + tw << tfr - tw << tfr + tw << 0.5;
        response << 1 << 1 << 0 << 0 << 1 << 1;
        weight << 1 << 1 << 1;
        type = BANDPASS;
        break;
    case 4:
        nbands = 1;
        tfl = ui->dsbDiffLowerBandEdge->value();
        tfr = ui->dsbDiffUpperBandEdge->value();
        //        slope = ui->dsbDiffDesiredSlope->value() ;
        bands << tfl << tfr;
        response << 0 << 1 << 0;
        weight << 1 << 1;
        type = DIFFERENTIATOR;
        break;
    case 5:
        nbands = 1;
        tfl = ui->dsbHilbertStart->value();
        tfr = ui->dsbHilbertStop->value();
        bands << tfl << tfr;
        response << 1;
        weight << 1;
        type = HILBERT;
        break;
    case 6:
        nbands = 2;
        tf = 1 / ui->sbChannels->value();
        bands << 0.0 << tf - tw << tf + tw << 0.5;
        response << 1 << 1 << 0 << 0;
        weight << 1 << 1;
        type = BANDPASS;
        break;
    default: statusBar()->showMessage( "Remez: <not implemented yet>", 2000 ); return;
    }

    Coefs.resize( nTaps );
    int err = remez( Coefs.data(), nTaps, nbands, bands.data(), response.data(), weight.data(), type, 64 );

    switch( err ) {
    case -1: statusBar()->showMessage( "Remez: failed to converge", 2000 ); break;
    case -2: statusBar()->showMessage( "Remez: insufficient extremals -- cannot continue", 2000 ); break;
    case -3: statusBar()->showMessage( "Remez: too many extremals -- cannot continue", 2000 ); break;
    }
}

void MainWindow::doRRC( void ) {
    Coefs.resize( nTaps );
    rootRaisedCosine( Coefs.data(), ui->dsbRRCSPS->value(), ui->dsbRRCBeta->value(), nTaps );
}

void MainWindow::doGauss( void ) {
    Coefs.resize( nTaps );
    gaussian( Coefs.data(), ui->dsbGSPS->value(), ui->dsbGBT->value(), nTaps );
}

void MainWindow::doLubber( void ) {
    Coefs.resize( nTaps );
    double K = ui->dsbLubberK->value();
    auto M = ui->sbChannels->value();

    if( !QmtxSDRLubber::tap_equation( Coefs.data(), M, nTaps / M, K ) )
        statusBar()->showMessage( "Lubber: Not enough taps selected", 2000 );
}


void MainWindow::findRoots( int nTaps ) {
    int newton_real( int n, const double coeff[], std::complex<double> res[] );

    double t[nTaps];
    for( int i = 0; i < nTaps; i += 1 )
        t[i] = Coefs[i];

    std::complex<double> zeroes[nTaps];
    newton_real( nTaps, t, zeroes );

    ZeroesReal.resize( nTaps );
    ZeroesImag.resize( nTaps );
    for( int i = 0; i < nTaps; i += 1 ) {
        ZeroesReal[i] = zeroes[i].real();
        ZeroesImag[i] = zeroes[i].imag();
    }
}

void MainWindow::resizeEvent( QResizeEvent* ) {
    if( customPlotZero )
        customPlotZero->setFixedWidth( customPlotZero->size().height() );
}

void MainWindow::setCoefsWidget( QVector<ld_t> coefs ) {
    auto length = coefs.size();
    double range = pow( 2, ui->dsbNBits->value() ) - 1;
    ui->twCoefs->clear();
    for( int i = 0; i < length; i += 1 ) {
        QStringList columns;
        columns << QString( "%1" ).arg( i );
        columns << QString( "%1" ).arg( trunc( coefs[i] * 1E9 ) / 1E9, 0, 'g', 9 );
        columns << QString( "%1" ).arg( trunc( coefs[i] * range ), 0, 'g', 9 );
        if( !Window.isEmpty() )
            columns << QString( "%1" ).arg( trunc( Window[i] * 1E9 ) / 1E9, 0, 'g', 9 );
        else
            columns << "";
        ui->twCoefs->addTopLevelItem( new QTreeWidgetItem( columns ) );
    }
}

void MainWindow::doShow( QVector<ld_t> coefs ) {
    makeFreqGraph();
    makeTimeGraph();
    makePoleGraph();

    auto length = coefs.size();
    if( length == 0 )
        return;

    // x axis
    QVector<double> x( length );
    for( int i = 0; i < length; i += 1 )
        x[i] = ld_t( i );

    // do time plots
    if( !Window.isEmpty() ) {
        QVector<double> w( length );
        for( int i = 0; i < length; i += 1 )
            w[i] = Window[i];
        if( customPlotTime )
            customPlotTime->graph( 0 )->setData( x, w );
    }

    if( !coefs.isEmpty() ) {
        QVector<double> t( length );
        for( int i = 0; i < length; i += 1 )
            t[i] = coefs[i];
        if( customPlotTime )
            customPlotTime->graph( 1 )->setData( x, t );
    }
    if( customPlotTime )
        customPlotTime->rescaleAxes();
    if( customPlotTime )
        customPlotTime->replot();


    // do freq char plot
    int FFTLEN = 4096;
    while( length >= FFTLEN )
        FFTLEN *= 2;

    QVector<double> x2( FFTLEN );
    for( int i = 0; i < FFTLEN / 2; i += 1 )
        x2[i] = double( i ) / FFTLEN;

    QVector<ld_t> F( 2 * FFTLEN );
    freqChar( coefs.data(), F.data(), length, FFTLEN );

    QVector<double> M( FFTLEN );
    magnitude( F.data(), M.data(), FFTLEN, isLogaritmic() );
    QVector<double> P( FFTLEN );
    phase( F.data(), P.data(), FFTLEN );
    QVector<double> D( FFTLEN );
    group( P.data(), D.data(), FFTLEN );

    if( customPlotFreq )
        customPlotFreq->graph( 0 )->setData( x2, M );
    //    if( customPlotFreq )
    //        customPlotFreq->graph( 0 )->rescaleKeyAxis();

    auto vRange = QCPRange( -0.2, 1.2 );
    if( isLogaritmic() )
        vRange = QCPRange( ui->dsbRangeLow->value(), ui->dsbRangeLow->value() );

    if( customPlotFreq )
        customPlotFreq->yAxis->setRange( vRange );

    if( customPlotFreq )
        customPlotFreq->replot();

    // do zeroes plot
    findRoots( length );
    if( customPlotZero )
        customPlotZero->graph( 0 )->setData( ZeroesReal, ZeroesImag );
    if( customPlotZero )
        customPlotZero->replot();

    ld_t absSum = 0.0;
    for( int i = 0; i < length; i += 1 )
        absSum += std::abs( coefs[i] );

    auto bitGrowth = std::ceil( std::log2( absSum ) );
    ui->sbBitGrowth->setValue( bitGrowth );
}

void MainWindow::on_actionLoad_Coefs_triggered() {
    QString fileName = QFileDialog::getOpenFileName(
        this, tr( "Load Coefficient File" ), loadSavePath, tr( "Xilinx coefficient files (*.coe);;All files (*.*)" ) );
    if( fileName.isEmpty() )
        return;

    loadSavePath = QDir::cleanPath( fileName );

    Coefs.clear();
    Window.clear();
    QFile file( fileName );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
        return;

    QTextStream stream( &file );
    if( fileName.endsWith( ".coe", Qt::CaseInsensitive ) ) {
        QString all;
        all = stream.readAll();
        QString::iterator s;
        int state = 0;
        double radix = 0.0;
        for( QString::iterator p = all.begin(); p != all.end(); ) {
            if( p->isLetter() ) {
                s = p;
                while( p != all.end() && p->isLetter() )
                    p += 1;
                QString word( s, p - s );
                if( state == 0 && word == "radix" )
                    state = -1;
                else if( state < 1 && word == "coefdata" )
                    state = 1;
                else {
                    statusBar()->showMessage( "Load coefs: syntax error", 2000 );
                    break;
                }
            } else if( p->isNumber() || *p == '+' || *p == '-' || *p == '.' ) {
                s = p;
                while( p != all.end() &&
                       ( p->isNumber() || *p == '+' || *p == '-' || *p == '.' || *p == 'e' || *p == 'E' ) )
                    p += 1;
                QString number( s, p - s );
                if( state == -1 )
                    radix = number.toDouble();
                else if( state == 1 )
                    Coefs.push_back( number.toDouble() );
            } else if( *p == ';' ) {
                if( state == 1 )
                    break;
                else
                    while( p != all.end() && p->isPrint() )
                        p += 1;
            } else if( *p == ',' ) {
                if( state == 1 )
                    p += 1;
                else {
                    statusBar()->showMessage( "Load coefs: syntax error", 2000 );
                    break;
                }
            } else
                p += 1;
        }
        file.close();

        // tap count
        nTaps = Coefs.size();
        if( nTaps <= 0 ) {
            statusBar()->showMessage( "Load coefs: load error", 2000 );
            return;
        }
        ui->sbNTaps->setValue( nTaps );

        //        // scale
        //        ld_t scale = 2.0L * fabsl( Coefs[nTaps / 2] );
        //        if( fabsl( scale ) > 1E-20 )
        //            for( int i = 0; i < nTaps; i++ )
        //                Coefs[i] /= scale;

        doShow( Coefs );
        setCoefsWidget( Coefs );

    } else
        statusBar()->showMessage( "Load coefs: <not implemented yet>", 2000 );
}

void MainWindow::on_actionSave_Coefs_triggered() {
    if( Coefs.isEmpty() )
        return;

    QString fileName = QFileDialog::getSaveFileName(
        this, tr( "Save Coefficient File" ), loadSavePath,
        tr( "Xilinx coefficient files (*.coe);;C/C++ include files (*.h);;C/C++ .inc files (*.inc);;VHDL files "
            "(*.vhd);;All files (*.*)" ) );
    if( fileName.isEmpty() )
        return;

    loadSavePath = QDir::cleanPath( fileName );

    QFile file( fileName );
    if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
        return;

    QVector<ld_t> coefs;
    coefs = Coefs;

    //    auto phases = ui->sbChannels->value();
    //    const auto length = Coefs.size();

    //    if( ui->twMethod->currentIndex() != 6 )
    //        coefs = Coefs;
    //    else {                        // reshaped polyphase
    //    }

    QDateTime date( QDateTime::currentDateTime() );
    QTextStream stream( &file );
    if( fileName.endsWith( ".coe", Qt::CaseInsensitive ) ) {
        stream << ";\n; Created by FIRTool at: " << date.toString( "MMMM d yyyy" )
               << "\n; c 2013-2021 www.mediatronix.com\n;\n";
        stream << "radix = 10 ;\n"
               << "coefdata =\n";
        if( ui->dsbNBits->value() > 0.5 ) {
            double range = pow( 2, ui->dsbNBits->value() ) - 1;
            for( int i = 0; i < coefs.size() - 1; i += 1 )
                stream << QString( "%1" ).arg( trunc( coefs[i] * range ), 0, 'g', 9 ) << ",\n";
            stream << QString( "%1" ).arg( trunc( coefs[coefs.size() - 1] * range ), 0, 'g', 9 ) << ";\n";
        } else {
            for( int i = 0; i < coefs.size() - 1; i += 1 )
                stream << QString( "%1" ).arg( coefs[i], 0, 'g', 9 ) << ",\n";
            stream << QString( "%1" ).arg( coefs[coefs.size() - 1], 0, 'g', 9 ) << " ;\n";
        }
    } else if( fileName.endsWith( ".h", Qt::CaseInsensitive ) ) {
        stream << "//\n// Created by FIRTool at: " << date.toString( "MMMM d yyyy" )
               << "\n// c 2013-2021 www.mediatronix.com\n//\n";
        stream << "double coefdata[] = {\n";
        for( int i = 0; i < coefs.size() - 1; i += 1 )
            stream << QString( "    %1" ).arg( coefs[i], 0, 'g', 9 ) << ",\n";
        stream << QString( "    %1" ).arg( coefs[coefs.size() - 1], 0, 'g', 9 ) << "\n} ;\n";
    } else if( fileName.endsWith( ".inc", Qt::CaseInsensitive ) ) {
        stream << "//\n// Created by FIRTool at: " << date.toString( "MMMM d yyyy" )
               << "\n// c 2013-2021 www.mediatronix.com\n//\n";
        if( ui->dsbNBits->value() > 0.5 ) {
            double range = pow( 2, ui->dsbNBits->value() ) - 1;
            for( int i = 0; i < coefs.size() - 1; i += 1 )
                stream << QString( "%1" ).arg( trunc( coefs[i] * range ), 0, 'g', 9 ) << ",\n";
            stream << QString( "%1" ).arg( trunc( coefs[coefs.size() - 1] * range ), 0, 'g', 9 ) << "\n";
        } else {
            for( int i = 0; i < coefs.size() - 1; i += 1 )
                stream << QString( "%1" ).arg( coefs[i], 0, 'g', 9 ) << ",\n";
            stream << QString( "%1" ).arg( coefs[coefs.size() - 1], 0, 'g', 9 ) << " ;\n";
        }
    } else if( fileName.endsWith( ".vhd", Qt::CaseInsensitive ) ) {
        double range = pow( 2, ui->dsbNBits->value() ) - 1;
        stream << "--\n-- Created by FIRTool at: " << date.toString( "MMMM d yyyy" )
               << "\n-- c 2013-2021 www.mediatronix.com\n--\n";
        stream << QString( "    signal coefdata : array( 0 to %1 ) of integer range %2 to %3 := (\n" )
                      .arg( trunc( coefs.size() - 1 ) )
                      .arg( -( int )( range / 2 ) )
                      .arg( ( int )( range / 2 ) );
        for( int i = 0; i < coefs.size() - 1; i += 1 )
            stream << QString( "        %1" ).arg( trunc( coefs[i] * range ), 0, 'g', 9 ) << ",\n";
        stream << QString( "        %1" ).arg( trunc( coefs[coefs.size() - 1] * range ), 0, 'g', 9 ) << "\n    ) ;";
    }
    file.close();
}

void MainWindow::closeEvent( QCloseEvent* event ) {
    writeSettings();
    event->accept();
}

void MainWindow::doUIenables( int type, int method ) {
    ui->gbEstimator->setEnabled( type < 3 && method < 10 );
}

void MainWindow::on_twType_currentChanged( int index ) {
    doUIenables( index, ui->twMethod->currentIndex() );
}

void MainWindow::on_twMethod_currentChanged( int index ) {
    doUIenables( ui->twType->currentIndex(), index );
}

void MainWindow::on_cbFilterType_currentIndexChanged( int index ) {
    ui->twType->setEnabled( index != 6 );

    ui->dsbSubtype->setEnabled( index == 0 || index == 1 );
    switch( index ) {
    case 0: ui->dsbSubtype->setPrefix( "M: " ); break;
    case 1: ui->dsbSubtype->setPrefix( "Type: " ); break;
    default: ui->dsbSubtype->setPrefix( "<not used> " ); break;
    }
}

void MainWindow::setTaps( int n ) {
    QSignalBlocker blocker( ui->sbNTaps );
    ui->sbNTaps->setValue( n );
}

void MainWindow::on_pbEstimate_clicked() {
    double tw = ui->dsbTW->value();
    double attn = ui->dsbAtten->value();
    double dp = -attn / 20.0;
    double ds = -attn / 20.0;
    int nTaps = 3;

    // various estimators
    switch( ui->twMethod->currentIndex() ) {
    case 0: {
        nTaps = ceil( 1 + acosh( 1 / pow( 10.0, dp ) ) / tw / 2 );
        double beta = cosh( acosh( pow( 10.0, attn / 20.0 ) ) / ( nTaps - 1 ) );
        ui->dsbChebyBeta->setValue( beta );
    } break;

    case 1: {
        double beta = 0.0;
        if( attn > 50.0 )
            beta = 0.1102 * ( attn - 8.7 );
        else if( attn >= 21.0 )
            beta = 0.5842 * pow( ( attn - 21.0 ), 0.4 ) + 0.07886 * ( attn - 21.0 );
        ui->dsbKaiserBeta->setValue( beta );
        nTaps = ceil( ( attn - 8.0 ) / ( 2.285 * tw ) );
    } break;

    default:
    case 2: {
        double Dinf =
            ds * ( 5.309e-3 * dp * dp + 7.114e-2 * dp + -4.761e-1 ) + -2.66e-3 * dp * dp + -5.941e-1 * dp + -4.278e-1;
        double f = 11.01217 + 0.51244 * ( dp - ds );
        nTaps = ceil( Dinf / tw - f * tw + 1 );
    } break;
    }

    setTaps( nTaps );
}

void MainWindow::on_actionNew_triggered() {
    projectFileName = "";
}

void MainWindow::on_actionOpen_triggered() {
    projectFileName = QFileDialog::getOpenFileName(
        0, "Open Project File", loadSavePath, "FIRTool project files (*.json);;All files (*.*)" );
    if( projectFileName.isEmpty() )
        return;

    QFile file( projectFileName );
    if( !file.open( QFile::ReadOnly ) )
        return;

    auto doc = QJsonDocument::fromJson( file.readAll() );
    auto settings = doc.object();

    auto typeGroup = settings["type"].toObject();
    ui->twType->setCurrentIndex( typeGroup["index"].toInt( 0 ) );
    ui->dsbLPStop->setValue( typeGroup["dsbLPStop"].toDouble( 0.25 ) );
    ui->dsbHPStart->setValue( typeGroup["dsbHPStart"].toDouble( 0.25 ) );
    ui->dsbBPStart->setValue( typeGroup["dsbBPStart"].toDouble( 0.15 ) );
    ui->dsbBPStop->setValue( typeGroup["dsbBPStop"].toDouble( 0.35 ) );
    ui->dsbBSStart->setValue( typeGroup["dsbBSStart"].toDouble( 0.15 ) );
    ui->dsbBSStop->setValue( typeGroup["dsbBSStop"].toDouble( 0.35 ) );

    auto methodGroup = settings["method"].toObject();
    ui->twMethod->setCurrentIndex( methodGroup["index"].toInt( 0 ) );

    ui->dsbChebyBeta->setValue( methodGroup["dsbChebyBeta"].toDouble( 1.001 ) );
    ui->dsbKaiserBeta->setValue( methodGroup["dsbKaiserBeta"].toDouble( 4.0 ) );

    ui->cbFilterType->setCurrentIndex( methodGroup["cbFilterType"].toInt( 0 ) );
    ui->dsbSubtype->setValue( methodGroup["dsbSubtype"].toDouble( 0 ) );

    ui->dsbRRCSPS->setValue( methodGroup["dsbRRCSPS"].toDouble( 3.0 ) );
    ui->dsbRRCBeta->setValue( methodGroup["dsbRRCBeta"].toDouble( 0.5 ) );

    ui->dsbGSPS->setValue( methodGroup["dsbGSPS"].toDouble( 3.0 ) );
    ui->dsbGBT->setValue( methodGroup["dsbGBT"].toDouble( 0.5 ) );

    ui->cbLog->setChecked( methodGroup["cbLog"].toBool( true ) );

    ui->sbNTaps->setValue( methodGroup["dsbNTaps"].toInt( 63 ) );
    ui->dsbNBits->setValue( methodGroup["dsbNBits"].toInt( 18 ) );
    ui->dsbAtten->setValue( methodGroup["dsbAtten"].toDouble( 100.0 ) );
    ui->dsbTW->setValue( methodGroup["dsbTW"].toDouble( 0.05 ) );
}

void MainWindow::on_actionSave_triggered() {
    if( projectFileName.isEmpty() ) {
        projectFileName = QFileDialog::getSaveFileName(
            0, "Save Project File", loadSavePath, "FIRTool project files (*.json);;All files (*.*)" );
        if( projectFileName.isEmpty() )
            return;
    }

    QJsonObject settings;

    QJsonObject typeGroup;
    typeGroup["index"] = ui->twType->currentIndex();
    typeGroup["dsbLPStop"] = ui->dsbLPStop->value();
    typeGroup["dsbHPStart"] = ui->dsbHPStart->value();
    typeGroup["dsbBPStart"] = ui->dsbBPStart->value();
    typeGroup["dsbBPStop"] = ui->dsbBPStop->value();
    typeGroup["dsbBSStart"] = ui->dsbBSStart->value();
    typeGroup["dsbBSStop"] = ui->dsbBSStop->value();
    settings["type"] = typeGroup;

    QJsonObject methodGroup;
    methodGroup["index"] = ui->twMethod->currentIndex();

    methodGroup["dsbChebyBeta"] = ui->dsbChebyBeta->value();
    methodGroup["dsbKaiserBeta"] = ui->dsbKaiserBeta->value();

    methodGroup["cbFilterType"] = ui->cbFilterType->currentIndex();
    methodGroup["dsbSubtype"] = ui->dsbSubtype->value();

    methodGroup["dsbRRCSPS"] = ui->dsbRRCSPS->value();
    methodGroup["dsbRRCBeta"] = ui->dsbRRCBeta->value();

    methodGroup["dsbGSPS"] = ui->dsbGSPS->value();
    methodGroup["dsbGBT"] = ui->dsbGBT->value();

    methodGroup["cbLog"] = ui->cbLog->isChecked();

    methodGroup["dsbNTaps"] = ui->sbNTaps->value();
    methodGroup["dsbNBits"] = ui->dsbNBits->value();
    methodGroup["dsbAtten"] = ui->dsbAtten->value();
    methodGroup["dsbTW"] = ui->dsbTW->value();
    settings["method"] = methodGroup;
}

void MainWindow::on_actionSave_As_triggered() {
    projectFileName = "";
    on_actionSave_triggered();
}

void MainWindow::on_cbLPHB_stateChanged( int arg1 ) {
    if( arg1 == Qt::Checked ) {
        ui->dsbLPStop->setValue( 0.25 );
        ui->dsbLPStop->setDisabled( true );
    } else
        ui->dsbLPStop->setEnabled( true );
}

void MainWindow::on_cbHPHB_stateChanged( int arg1 ) {
    if( arg1 == Qt::Checked ) {
        ui->dsbHPStart->setValue( 0.25 );
        ui->dsbHPStart->setDisabled( true );
    } else
        ui->dsbHPStart->setEnabled( true );
}

void MainWindow::on_cbLog_toggled( bool checked ) {
    doShow( Coefs );

    ui->dsbRangeLow->setEnabled( checked );
    ui->dsbRangeHigh->setEnabled( checked );
}

void MainWindow::on_dsbRangeLow_valueChanged( double ) {
    doShow( Coefs );
}

void MainWindow::on_dsbRangeHigh_valueChanged( double ) {
    doShow( Coefs );
}

void MainWindow::on_dsbXRangeLow_valueChanged( double ) {
    doShow( Coefs );
}

void MainWindow::on_dsbXRangeHigh_valueChanged( double ) {
    doShow( Coefs );
}

void MainWindow::on_tbFreqHome_clicked() {
    ui->dsbXRangeLow->setValue( 0.0 );
    ui->dsbXRangeHigh->setValue( 0.5 );
    if( isLogaritmic() ) {
        ui->dsbRangeLow->setValue( -100 );
        ui->dsbRangeHigh->setValue( 10 );
    } else {
        ui->dsbRangeLow->setValue( 0.0 );
        ui->dsbRangeHigh->setValue( 1.0 );
    }
}

void MainWindow::on_pbReordered_clicked( bool ) {
    if( Coefs.size() & 1 )          // kludge
        Coefs.takeLast();

    auto phases = ui->sbChannels->value();
    Coefs = reshape( Coefs, phases, false /*ui->cbReordered->isChecked()*/, false );
    doShow( Coefs );
    setCoefsWidget( Coefs );
}

void MainWindow::on_twCoefs_itemSelectionChanged( void ) {
    auto items = ui->twCoefs->selectedItems();
    if( items.isEmpty() ) {
        doShow( Coefs );
        return;
    }

    QVector<ld_t> coefs;
    for( auto item : items ) {
        auto index = ui->twCoefs->indexOfTopLevelItem( item );
        coefs.push_back( Coefs[index] );
    }
    doShow( coefs );
}
