
/*
    copyright © 2003 - 2020 : Henk van Kampen <henk@mediatronix.com>

    This file is part of firtool.

    firtool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    firtool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with firtool.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>

#include "fftw.h"
#include "windowCheby.h"

static void cheb( fftwl_complex y[], ld_t beta, int n ) {
    for( int i = 0; i < n; i += 1, y++ ) {
        ld_t a = fabsl( beta * cosl( ( ld_t )M_PI * i / n ) );
        if( a > 1.0L )
            ( *y )[0] = powl( -1.0L, i ) * coshl( acoshl( a ) * n );
        else
            ( *y )[0] = powl( -1.0L, i ) * cosl( acosl( a ) * n );
        ( *y )[1] = 0.0;
    }
}

void chebyshev( ld_t* W, int aLen, double beta ) {
    if( aLen < 0 )
        return;
    else if( aLen == 1 )
        W[0] = 1;
    else {
        int M = aLen - 1;

        fftwl_complex y[aLen];
        cheb( y, beta, M );

        fftwl_complex z[aLen];
        fftb( y, z, M );

        z[0][0] = z[0][0] / 2;
        z[M][0] = z[0][0];
        for( int i = 0; i < aLen; i += 1 )
            W[i] = z[i][0] / z[M / 2][0];
    }
}
