
/*
    Copyright © 2003..2021 : Henk van Kampen <henk@mediatronix.com>

    This file is part of firtool.

    firtool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    firtool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with firtool.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "double.h"

#include <cmath>
#include <complex>
#include <valarray>
#include <vector>


using namespace std;

typedef std::complex<ld_t> Complex;
typedef std::valarray<Complex> CArray;

class QmtxSDRLubber {
   public:
    static bool tap_equation( ld_t* T, const int M, const int L, ld_t K );

   private:
    static void ifft( CArray& x );
    static void fft( CArray& x );
};
