
/*
    copyright © 2003 - 2020 : Henk van Kampen <henk@mediatronix.com>

    This file is part of firtool.

    firtool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    firtool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with firtool.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "double.h"

void lowPass( ld_t* T, double stop_freq, ld_t* W, int aLen );
void highPass( ld_t* T, double start_freq, ld_t* W, int aLen );
void bandPass( ld_t* T, double start_freq, double stop_freq, ld_t* W, int aLen );
void bandStop( ld_t* T, double stop_freq, double start_freq, ld_t* W, int aLen );
void differentiator( ld_t* T, double start_freq, double stop_freq, ld_t* W, int aLen );
void gaussian( ld_t* T, double spb, double bt, int nLen );
void rootRaisedCosine( ld_t* T, double spb, double alpha, int nLen );
