
CONFIG(release, debug|release) {
    message( "AppImageBuild" )

    LINUXDEPLOYQT_URL = https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage
    LINUXDEPLOYQT = ./linuxdeployqt-continuous-x86_64.AppImage

    APPIMAGETOOL_URL = https://github.com/AppImage/AppImageKit/releases/download/10/appimagetool-x86_64.AppImage
    APPIMAGETOOL = ./appimagetool-x86_64.AppImage

    linuxdeployqt.target = $$LINUXDEPLOYQT
    linuxdeployqt.commands = \
        wget -O $$LINUXDEPLOYQT $$LINUXDEPLOYQT_URL && \
        chmod +x $$LINUXDEPLOYQT

    appimagetool.target = $$APPIMAGETOOL
    appimagetool.commands = \
        wget -O $$APPIMAGETOOL $$APPIMAGETOOL_URL && \
        chmod +x $$APPIMAGETOOL

    appimage.target = FIRTool.AppImage
    appimage.depends = linuxdeployqt appimagetool
    appimage.commands = \
        pwd && \
        rm -rf AppImageBuild && \
        mkdir -p AppImageBuild && \
        cp FIRTool AppImageBuild/ && \
        cp $$PWD/templates/appimage/default.desktop AppImageBuild/ && \
        mkdir -p AppImageBuild/usr/share/icons/hicolor/ && \
        cp -r $$PWD/templates/icons/* AppImageBuild/usr/share/icons/hicolor/ && \
        cp -r $$PWD/templates/icons/16x16/apps/firtool.png AppImageBuild/ && \
        LD_LIBRARY_PATH="/home/henk/prefix/default/lib:/home/henk/prefix/default/lib64/:$LD_LIBRARY_PATH" \
        $$LINUXDEPLOYQT AppImageBuild/FIRTool -verbose=2 -appimage -qmake=$$QMAKE_QMAKE -bundle-non-qt-libs

    super.depends = FIRTool linuxdeployqt appimagetool appimage
    QMAKE_EXTRA_TARGETS += linuxdeployqt appimagetool appimage super
}
