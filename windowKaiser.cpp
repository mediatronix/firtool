
/*
    copyright © 2003 - 2020 : Henk van Kampen <henk@mediatronix.com>

    This file is part of firtool.

    firtool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    firtool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with firtool.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "windowKaiser.h"
#include "math.h"

static ld_t I0( ld_t x ) {
    ld_t d = 0.0;
    ld_t ds = 1.0;
    ld_t s = 1.0;
    do {
        d += 2.0L;
        ds *= x * x / ( d * d );
        s += ds;
    } while( ds > s * 1E-300L );
    return s;
}

void kaiser( ld_t* W, int aLen, double beta ) {
    ld_t den = I0( M_PI * beta );
    int nOn2 = aLen / 2;

    W[nOn2] = 1;
    for( int j = 1; j < nOn2; j += 1 ) {
        double t = I0( ( ld_t )M_PI * beta * sqrt( 1.0L - ( ld_t )( j * j ) / ( ld_t )( nOn2 * nOn2 ) ) );
        W[nOn2 + j] = W[nOn2 - j] = 1 * t / den;
    }
}
