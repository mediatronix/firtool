/*
    Copyright © 2003..2021 : Henk van Kampen <henk@mediatronix.com>

    This file is part of firtool.

    firtool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    firtool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with firtool.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QFile>
#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>

/*
    copyright © 2003 - 2020 : Henk van Kampen <henk@mediatronix.com>

    This file is part of firtool.

    firtool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    firtool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with firtool.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QSettings>
#include <QVector>

#include "double.h"
#include "filters.h"
#include "freqchar.h"
#include "remez.h"

#include <windowCheby.h>
#include <windowHarris.h>
#include <windowKaiser.h>
#include <windowRifeVincent.h>
#include "qmtxsdrlubber.h"

#include "qcustomplot.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

   public:
    explicit MainWindow( QWidget* parent = 0 );
    ~MainWindow( void );

    void closeEvent( QCloseEvent* event );
    void findRoots( int nTaps );

   protected:
    void resizeEvent( QResizeEvent* );          // virtual

   private slots:
    void on_actionDesign_triggered( void );
    void on_pbEstimate_clicked( void );
    void on_actionAbout_triggered( void );

    void on_twType_currentChanged( int index );
    void on_cbFilterType_currentIndexChanged( int index );
    void on_twMethod_currentChanged( int );

    void on_actionLoad_Coefs_triggered( void );
    void on_actionSave_Coefs_triggered( void );

    void on_actionNew_triggered( void );
    void on_actionOpen_triggered( void );
    void on_actionSave_triggered( void );
    void on_actionSave_As_triggered( void );

    void on_cbLPHB_stateChanged( int arg1 );
    void on_cbHPHB_stateChanged( int arg1 );

    void on_dsbRangeLow_valueChanged( double );
    void on_dsbRangeHigh_valueChanged( double );

    void on_cbLog_toggled( bool checked );

    void updateYRangeBoxes( QCPRange newRange );
    void updateXRangeBoxes( QCPRange newRange );

    void on_dsbXRangeLow_valueChanged( double arg1 );

    void on_dsbXRangeHigh_valueChanged( double arg1 );

    void on_tbFreqHome_clicked();

    void on_pbReordered_clicked( bool );

    void on_twCoefs_itemSelectionChanged();

   private:
    void doCheby( void );
    void doKaiser( void );
    void doWindowed( void );
    void doRemez( void );
    void doRRC( void );
    void doGauss( void );
    void doLubber( void );

    void oddTaps( void );
    void setTaps( int n );

    void doShow( QVector<ld_t> coefs );
    void makeFreqGraph( void );
    void makeTimeGraph( void );
    void makePoleGraph( void );

    void doUIenables( int type, int method );
    bool isLogaritmic( void );

    void readSettings( void );
    void writeSettings( void );

    Ui::MainWindow* ui;
    QPointer<QCustomPlot> customPlotFreq;
    QPointer<QCustomPlot> customPlotTime;
    QPointer<QCustomPlot> customPlotZero;
    QSharedPointer<QCPAxisTickerFixed> fixedTicker;

    QString loadSavePath;
    QString projectFileName;

    int nTaps{ 63 };
    QVector<ld_t> Coefs;
    QVector<ld_t> Window;
    QVector<double> ZeroesReal;
    QVector<double> ZeroesImag;

   private:
    QVector<ld_t> reshape( const QVector<ld_t> V, const int m, bool reordered, bool rev );
    void setCoefsWidget( QVector<ld_t> coefs );
};
